package com.example.bluetooth.le;

import static android.bluetooth.BluetoothGattCharacteristic.FORMAT_SINT8;
import static android.bluetooth.BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE;

import java.util.List;
import java.util.UUID;

import android.app.Activity;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattServerCallback;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.content.Intent;
import android.graphics.Matrix;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

public class ConnectDatosActivity extends Activity {
	private String mDeviceName;
	private String mDeviceAddress;
	public boolean mWorking;
	public static final UUID CCC = UUID.fromString("00002902-0000-1000-8000-00805f9b34fb");
	private SampleGattAttributes atributeManager;
	public final WriteQueue writeQueue = new WriteQueue();
	public static final String EXTRAS_DEVICE_NAME = "DEVICE_NAME";
	public static final String EXTRAS_DEVICE_ADDRESS = "DEVICE_ADDRESS";
	private BluetoothManager mBluetoothManager;
	private BluetoothGatt mBluetoothGatt;
	private BluetoothDevice sensor;
	private boolean visible;
	int periodo=500;
	
	float xx=0;
	float yy=0;
	float zz=0;
	float ax=0;
	float ay=0;
	float az=0;	
	TextView lblAcelerometro;
	EditText txtPeriodo;
	TextView lblGiroscopio;
	TextView lblMagnetometro;
	TextView lblBarometro;
	TextView lblTemperatura;
	TextView lblHumedad;
	TextView lblBotones;
	RelativeLayout layoutRel;
	ImageView vImagen;
    private ToggleButton togle;
    private ToggleButton togleFoto;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		final Intent intent = getIntent();
		atributeManager=new SampleGattAttributes(getApplicationContext());
		mDeviceName = intent.getStringExtra(EXTRAS_DEVICE_NAME);
		mWorking=false;
		mDeviceAddress = intent.getStringExtra(EXTRAS_DEVICE_ADDRESS);
		setContentView(R.layout.connectdatos);
		TextView dispositivo=(TextView)findViewById(R.id.lblDispositivo);
		dispositivo.setText(mDeviceName);
		visible=false;
		lblAcelerometro=(TextView) findViewById(R.id.lbl_acelerometro);
		lblGiroscopio=(TextView) findViewById(R.id.lbl_giroscopio);
		lblMagnetometro=(TextView) findViewById(R.id.lbl_magnetometro);
		lblBarometro=(TextView) findViewById(R.id.lbl_barometro);
		lblTemperatura=(TextView) findViewById(R.id.lbl_temperatura);
		lblHumedad=(TextView) findViewById(R.id.lbl_humedad);
		lblBotones=(TextView) findViewById(R.id.lbl_botones);
		txtPeriodo=(EditText)findViewById(R.id.txt_periodo);
		layoutRel=(RelativeLayout)findViewById(R.id.imageView);
		vImagen=(ImageView)findViewById(R.id.imageResize);
        togle=(ToggleButton) findViewById(R.id.toggle);
        togleFoto=(ToggleButton) findViewById(R.id.toggleFoto);
	}
	@Override
	protected void onPause() {
		if (mBluetoothGatt!=null){
			mBluetoothGatt.close();
			mBluetoothGatt = null;
		}
	    	
		super.onPause();
	}
	@Override
	protected void onResume() {
		mBluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
		mBluetoothManager.openGattServer(getApplicationContext(), getBTCallbackServer());
		super.onResume();
	}

	 @Override
	    public boolean onCreateOptionsMenu(Menu menu) {
	        getMenuInflater().inflate(R.menu.conectdatos, menu);
	        if (!mWorking) {
	            menu.findItem(R.id.menu_refresh).setActionView(null);
	        } else {
	            menu.findItem(R.id.menu_refresh).setActionView(
	                    R.layout.actionbar_indeterminate_progress);
	        }
	        return true;
	    }
	
	
	 public void clickImagen(View target){
		 visible=true;
		 if (togleFoto.isChecked()){
			 vImagen.setImageResource(R.drawable.logo_negro);
		 }else{
			 vImagen.setImageResource(R.drawable.glass_team_pano);
		 }
		 layoutRel.setVisibility(View.VISIBLE);
		 
	 }
	 public void clickClose(View target){
		 visible=false;
		 layoutRel.setVisibility(View.GONE);
		 vImagen.setRotation(0);
		 vImagen.setTranslationX(0);
		 vImagen.setScaleX((float) 1.0);
		 vImagen.setScaleY((float) 1.0);
		 vImagen.setImageMatrix(new Matrix()); 
	 }
	 
	 public void rotateScale(double x,double y, double z){
		 if (visible){
			 float degrees = (float) (90.0*x);
			 float zoom=(float) ((-z*0.5)+1);
			// float valor=(float) (vImagen.getWidth()/x*90.0);
			 //Log.d("ROTATE", "x:"+x+"  y:"+y+" z:"+z+"  d:"+degrees);
			 if (togle.isChecked()){
				 vImagen.setRotation(degrees);
			 }else{
				 
				 vImagen.setTranslationX(degrees*4);
			 }
			
			 vImagen.setScaleX(zoom);
			 vImagen.setScaleY(zoom);
			 int mediador=(int) (ay*2);
			 Log.d("BTLA", "Giro["+ay+"]   ->["+mediador+"]");
			 //layoutRel.setLeft(mediador);
			// vImagen.setTranslationX(mediador);
			 /*	 Matrix matrix=new Matrix();
			 vImagen.setScaleType(ScaleType.MATRIX);   //required
			 matrix.postRotate(degrees);
			 vImagen.setImageMatrix(new Matrix());
			 vImagen.invalidate();*/
		 }
	 }
	 
	public void clickCalibrate(View target){
		xx=ax=(float) 0.0;
		yy=ay=(float) 0.0;
		zz=az=(float) 0.0;
	}
	 
	public void onConectar(View target){
		mWorking=true;
		invalidateOptionsMenu();
		String peri=txtPeriodo.getText().toString();
		int nuevoPeriodo=Integer.parseInt(peri);
		if ((nuevoPeriodo>1270)||(nuevoPeriodo<10)){
			nuevoPeriodo=500;
		}
		periodo=nuevoPeriodo;
		sensor=mBluetoothManager.getAdapter().getRemoteDevice(mDeviceAddress);
		mBluetoothGatt =sensor.connectGatt(getApplicationContext(), true, getBTCallbackDevice());
	}


	public BluetoothGattServerCallback getBTCallbackServer(){
		BluetoothGattServerCallback btCallback=new BluetoothGattServerCallback() {

			@Override
			public void onCharacteristicReadRequest(BluetoothDevice device,
					int requestId, int offset,
					BluetoothGattCharacteristic characteristic) {
				super.onCharacteristicReadRequest(device, requestId, offset, characteristic);
				Log.d("BTLE", "getBTCallbackServer:onCharacteristicReadRequest");
			}

			@Override
			public void onCharacteristicWriteRequest(
					BluetoothDevice device, int requestId,
					BluetoothGattCharacteristic characteristic,
					boolean preparedWrite, boolean responseNeeded,
					int offset, byte[] value) {
				super.onCharacteristicWriteRequest(device, requestId, characteristic,
						preparedWrite, responseNeeded, offset, value);
				Log.d("BTLE", "getBTCallbackServer:onCharacteristicWriteRequest");
			}

			@Override
			public void onConnectionStateChange(BluetoothDevice device,
					int status, int newState) {
				super.onConnectionStateChange(device, status, newState);
				Log.d("BTLE", "getBTCallbackServer: onConnectionStateChange");

			}

			@Override
			public void onDescriptorReadRequest(BluetoothDevice device,
					int requestId, int offset,
					BluetoothGattDescriptor descriptor) {
				super.onDescriptorReadRequest(device, requestId, offset, descriptor);
				Log.d("BTLE", "getBTCallbackServer: onDescriptorReadRequest");

			}

			@Override
			public void onDescriptorWriteRequest(BluetoothDevice device,
					int requestId, BluetoothGattDescriptor descriptor,
					boolean preparedWrite, boolean responseNeeded,
					int offset, byte[] value) {
				super.onDescriptorWriteRequest(device, requestId, descriptor, preparedWrite,
						responseNeeded, offset, value);
				Log.d("BTLE", "getBTCallbackServer: onDescriptorWriteRequest");

			}

			@Override
			public void onExecuteWrite(BluetoothDevice device,
					int requestId, boolean execute) {
				super.onExecuteWrite(device, requestId, execute);
				Log.d("BTLE", "getBTCallbackServer: onExecuteWrite");

			}

			@Override
			public void onServiceAdded(int status,
					BluetoothGattService service) {
				super.onServiceAdded(status, service);
				Log.d("BTLE", "getBTCallbackServer: onServiceAdded");
			}

		};

		return btCallback;
	}


	public BluetoothGattCallback getBTCallbackDevice(){
		BluetoothGattCallback btCallback=new BluetoothGattCallback() {

			@Override
			public void onCharacteristicChanged(BluetoothGatt gatt,
					BluetoothGattCharacteristic characteristic) {
				super.onCharacteristicChanged(gatt, characteristic);
		//		Log.d("BTLE", "getBTCallbackDevice: onCharacteristicChanged");
				DiggestCharacteristicChange(gatt, characteristic);

			}

			@Override
			public void onCharacteristicRead(BluetoothGatt gatt,
					BluetoothGattCharacteristic characteristic, int status) {
				super.onCharacteristicRead(gatt, characteristic, status);
				Log.d("BTLE", "getBTCallbackDevice: onCharacteristicRead");

			}

			@Override
			public void onCharacteristicWrite(BluetoothGatt gatt,
					BluetoothGattCharacteristic characteristic, int status) {
				super.onCharacteristicWrite(gatt, characteristic, status);
				Log.d("BTLE", "getBTCallbackDevice: onCharacteristicWrite::"+characteristic.getUuid().toString());
				writeQueue.issue();

			}

			@Override
			public void onConnectionStateChange(BluetoothGatt gatt,
					int status, int newState) {
				super.onConnectionStateChange(gatt, status, newState);
				Log.d("BTLE", "getBTCallbackDevice: onConnectionStateChange");
				if (newState == BluetoothProfile.STATE_CONNECTED) {
					Log.d("BTLE", "getBTCallbackDevice: conectado, buscando servicios");
					mBluetoothGatt.discoverServices();						 
				}
			}

			@Override
			public void onDescriptorRead(BluetoothGatt gatt,
					BluetoothGattDescriptor descriptor, int status) {
				super.onDescriptorRead(gatt, descriptor, status);
				Log.d("BTLE", "getBTCallbackDevice: onDescriptorRead");

			}

			@Override
			public void onDescriptorWrite(BluetoothGatt gatt,
					BluetoothGattDescriptor descriptor, int status) {
				super.onDescriptorWrite(gatt, descriptor, status);
				Log.d("BTLE", "getBTCallbackDevice: onDescriptorWrite");
				writeQueue.issue();

			}

			@Override
			public void onReadRemoteRssi(BluetoothGatt gatt, int rssi,
					int status) {
				super.onReadRemoteRssi(gatt, rssi, status);
				Log.d("BTLE", "getBTCallbackDevice: onReadRemoteRssi");

			}

			@Override
			public void onReliableWriteCompleted(BluetoothGatt gatt,
					int status) {
				super.onReliableWriteCompleted(gatt, status);
				Log.d("BTLE", "getBTCallbackDevice: onReliableWriteCompleted");

			}

			@Override
			public void onServicesDiscovered(BluetoothGatt gatt, int status) {
				super.onServicesDiscovered(gatt, status);
				Log.d("BTLE", "getBTCallbackDevice: onServicesDiscovered");
				ConfigureGattService(gatt, status);

			}



		};

		return btCallback;
	}

	
	private void ConfigureGattService(BluetoothGatt gatt, int status) {
		List<BluetoothGattService> services = gatt.getServices();
		for (BluetoothGattService ser: services){
			String uuid=ser.getUuid().toString();
			String miservicio=atributeManager.lookup(uuid, "");
			//				Log.d("BTLE", "Configurar :"+miservicio);
			if (miservicio.equals("IR temperature service UUID")){
		//		Log.d("BTLE", "Configurar IR temperature");
				configurTempService(ser);
			}else if (miservicio.equals("Accelerometer service UUID")){
				//					Log.d("BTLE", "Configurar Accelerometer service UUID");
				configureAcellService(ser);
			}else if (miservicio.equals("Humidity service UUID")){
				//					Log.d("BTLE", "Configurar Accelerometer service UUID");
				//	configureAcellService(ser);
			}else if (miservicio.equals("Magnetometer service UUID")){
				//				Log.d("BTLE", "Configurar Accelerometer service UUID");
				//	configureAcellService(ser);
			}else if (miservicio.equals("Barometer service UUID")){
				//				Log.d("BTLE", "Configurar Accelerometer service UUID");
				//	configureAcellService(ser);
			}else if (miservicio.equals("Gyroscope service UUID")){
				//					Log.d("BTLE", "Configurar Accelerometer service UUID");
				configureGyroService(ser);
			}else if (miservicio.equals("botones service")){
				Log.d("BTLE", "Configurar DEFAULT service UUID");
				configureDefaultService(ser);
			}
		}
		mWorking=false;
		invalidateOptionsMenu();
	}

	

	protected void DiggestCharacteristicChange(BluetoothGatt gatt,
			BluetoothGattCharacteristic characteristic) {
		String servicio=atributeManager.lookup(characteristic.getUuid().toString(), "");
	//	Log.d("BTLE", "Recibidio:"+servicio);
		if (servicio.equals("IR temperature data UUID")){
//			Log.d("BTLE", "temperatura recibida");
			digestTemperatura(characteristic);
		}else if (servicio.equals("Accelerometer data UUID")){
			//					Log.d("BTLE", "Configurar Accelerometer service UUID");
			digestAcelerometer(characteristic);
		}else if (servicio.equals("Humidity data UUID")){
			//					Log.d("BTLE", "Configurar Accelerometer service UUID");
			//	configureAcellService(ser);
		}else if (servicio.equals("Magnetometer data UUID")){
			//				Log.d("BTLE", "Configurar Accelerometer service UUID");
			//	configureAcellService(ser);
		}else if (servicio.equals("Barometer data UUID")){
			//				Log.d("BTLE", "Configurar Accelerometer service UUID");
			//	configureAcellService(ser);
		}else if (servicio.equals("Gyroscope data UUID")){
			//					Log.d("BTLE", "Configurar Accelerometer service UUID");
				digestGyro(characteristic);
		}else if (servicio.equals("botones data")){
			Log.d("BTLE","Botones recibido");
			digestBotones(characteristic);
		}
	}

	

	

	private void digestAcelerometer(BluetoothGattCharacteristic c) {
		Integer x = c.getIntValue(FORMAT_SINT8, 0);
	      Integer y = c.getIntValue(FORMAT_SINT8, 1);
	      Integer z = c.getIntValue(FORMAT_SINT8, 2) * -1;

	     final double scaledX = x / 64.0;
	     final double scaledY = y / 64.0;
	     final double scaledZ = z / 64.0;
		    double[] roll=getRoll(x, y, z);
			Log.d("BTLE","-->["+roll[0]+"]["+roll[1]+"] digestAcel:");

		    runOnUiThread(new Runnable() {
			    @Override
			    public void run() {
			      lblAcelerometro.setText("X["+String.format("%.1f", scaledX)+"]" +
			      		" Y["+String.format("%.1f", scaledY)+"]" +
			      		" Z["+String.format("%.1f", scaledZ)+"]"); 
			      rotateScale(scaledX, scaledY, scaledZ);
			      }
			});
		
	}
	private void configureAcellService(BluetoothGattService ser) {
		writeQueue.queueRunnable(new Runnable() {
		      @Override
		      public void run() {
				BluetoothGattService serv=mBluetoothGatt.getService(UUID.fromString("f000aa10-0451-4000-b000-000000000000"));
		        BluetoothGattCharacteristic config = serv.getCharacteristic(UUID.fromString("f000aa12-0451-4000-b000-000000000000"));
		        byte[] code = new byte[] { 1 };
		        boolean successLocalySetValue = config.setValue(code);
		        boolean success = mBluetoothGatt.writeCharacteristic(config);
				Log.d("BTLE", "escrita notificacion ACELL"+success+"--"+successLocalySetValue);

		      }
		    });
		writeQueue.queueRunnable(new Runnable() {
		      @Override
		      public void run() {
				BluetoothGattService serv=mBluetoothGatt.getService(UUID.fromString("f000aa10-0451-4000-b000-000000000000"));
		        BluetoothGattCharacteristic config = serv.getCharacteristic(UUID.fromString("f000aa13-0451-4000-b000-000000000000"));
		        byte[] code = new byte[] { (byte) (periodo/10) };
		        boolean successLocalySetValue = config.setValue(code);
		        boolean success = mBluetoothGatt.writeCharacteristic(config);
				Log.d("BTLE", "escrita periodo ACELL"+success+"--"+successLocalySetValue);

		      }
		    });
		writeQueue.queueRunnable(new Runnable() {
			public void run() {
				BluetoothGattService serv=mBluetoothGatt.getService(UUID.fromString("f000aa10-0451-4000-b000-000000000000"));
				BluetoothGattCharacteristic config =serv.getCharacteristic(UUID.fromString("f000aa11-0451-4000-b000-000000000000"));
				boolean ff=mBluetoothGatt.setCharacteristicNotification(config, true);
	//			Log.d("BTLE", "NOTIF:TEMPERATURA:"+ff);
				BluetoothGattDescriptor configurar = config.getDescriptor(CCC);
				byte[] configValue = ENABLE_NOTIFICATION_VALUE;
				boolean success = configurar.setValue(configValue);
				Log.d("BTLE", "escrita notificacion GYRO"+success);
				mBluetoothGatt.writeDescriptor(configurar);
			}
		});	
	}
	
	private void configureGyroService(BluetoothGattService ser) {
		writeQueue.queueRunnable(new Runnable() {
		      @Override
		      public void run() {
				BluetoothGattService serv=mBluetoothGatt.getService(UUID.fromString("f000aa50-0451-4000-b000-000000000000"));
		        BluetoothGattCharacteristic config = serv.getCharacteristic(UUID.fromString("f000aa52-0451-4000-b000-000000000000"));
		        byte[] code = new byte[] { 7 };
		        boolean successLocalySetValue = config.setValue(code);
		        boolean success = mBluetoothGatt.writeCharacteristic(config);
				Log.d("BTLE", "escrita notificacion TEMP"+success+"--"+successLocalySetValue);

		      }
		    });
		writeQueue.queueRunnable(new Runnable() {
			public void run() {
				BluetoothGattService serv=mBluetoothGatt.getService(UUID.fromString("f000aa50-0451-4000-b000-000000000000"));
				BluetoothGattCharacteristic config =serv.getCharacteristic(UUID.fromString("f000aa51-0451-4000-b000-000000000000"));
				boolean ff=mBluetoothGatt.setCharacteristicNotification(config, true);
	//			Log.d("BTLE", "NOTIF:TEMPERATURA:"+ff);
				BluetoothGattDescriptor configurar = config.getDescriptor(CCC);
				byte[] configValue = ENABLE_NOTIFICATION_VALUE;
				boolean success = configurar.setValue(configValue);
				Log.d("BTLE", "escrita notificacion GYRO"+success);
				mBluetoothGatt.writeDescriptor(configurar);
			}
		});		
	}
	
	public double[] getRoll(float x, float y, float z){
		double roll=0;
		float alpha=(float) 0.5;
		xx = (float) (x * alpha + (xx * (1.0 - alpha)));
	    yy = (float) (y * alpha + (yy * (1.0 - alpha)));
	    zz = (float) (z * alpha + (zz * (1.0 - alpha)));
	 
	    //Roll & Pitch Equations
	    roll  = ((Math.atan2(-yy, zz)*180.0)/Math.PI);
	    double pitch =  ((Math.atan2(xx, Math.sqrt(yy*yy + zz*zz))*180.0)/Math.PI);
	    double[] ret={roll,pitch};
		return ret;
	}
	
	private void digestGyro(BluetoothGattCharacteristic characteristic) {
		final float y = BTUtils.shortSignedAtOffset(characteristic, 0) * (500f / 65536f) * -1;
	    final float x =BTUtils.shortSignedAtOffset(characteristic, 2) * (500f / 65536f);
	    final float z = BTUtils.shortSignedAtOffset(characteristic, 4) * (500f / 65536f);
	    ax=ax+x;
	    ay=ay+y;
	    az=az+z;
	//    Log.d("BTLA","x["+ax+"] y["+ay+"] z["+az+"]");
	    runOnUiThread(new Runnable() {
		    @Override
		    public void run() {
		      lblGiroscopio.setText("X["+String.format("%.1f", x)+"]" +
		      		" Y["+String.format("%.1f", y)+"]" +
		      		" Z["+String.format("%.1f", z)+"]"); 
		      }
		});
		
	}
	
	
	private void digestTemperatura(BluetoothGattCharacteristic characteristic){
		
		final double  hambient=BTUtils.extractAmbientTemperature(characteristic);
		final double target=BTUtils.extractTargetTemperature(characteristic, hambient);
		runOnUiThread(new Runnable() {
		    @Override
		    public void run() {
		      lblTemperatura.setText("ex["+String.format("%.2f", hambient)+"]C int["+String.format("%.2f", target)+"]C"); 
		      }
		});
	}
	
	
	private void configurTempService(BluetoothGattService ser) {
		writeQueue.queueRunnable(new Runnable() {
		      @Override
		      public void run() {
				BluetoothGattService serv=mBluetoothGatt.getService(UUID.fromString("f000aa00-0451-4000-b000-000000000000"));
		        BluetoothGattCharacteristic config = serv.getCharacteristic(UUID.fromString("f000aa02-0451-4000-b000-000000000000"));
		        byte[] code = new byte[] { 1 };
		        boolean successLocalySetValue = config.setValue(code);
		        boolean success = mBluetoothGatt.writeCharacteristic(config);
				Log.d("BTLE", "escrita notificacion TEMP"+success+"--"+successLocalySetValue);

		      }
		    });
		writeQueue.queueRunnable(new Runnable() {
			public void run() {
				BluetoothGattService serv=mBluetoothGatt.getService(UUID.fromString("f000aa00-0451-4000-b000-000000000000"));
				BluetoothGattCharacteristic config =serv.getCharacteristic(UUID.fromString("f000aa01-0451-4000-b000-000000000000"));
				boolean ff=mBluetoothGatt.setCharacteristicNotification(config, true);
				Log.d("BTLE", "NOTIF:TEMPERATURA:"+ff);
				BluetoothGattDescriptor configurar = config.getDescriptor(CCC);
				byte[] configValue = ENABLE_NOTIFICATION_VALUE;
				boolean success = configurar.setValue(configValue);
				Log.d("BTLE", "escrita notificacion TEMP"+success);
				mBluetoothGatt.writeDescriptor(configurar);
			}
		});
	}

	
	/*
	 * Configuracion y digest de botones
	 * 
	 * 
	 * */

	private void configureDefaultService(BluetoothGattService ser) {
		for (BluetoothGattCharacteristic car: ser.getCharacteristics()){
			String miservicio=atributeManager.lookup(car.getUuid().toString(), "");
			Log.d("BTLE", "--->caracteristica::"+miservicio);
			if (miservicio.equals("botones data")){
				writeQueue.queueRunnable(new Runnable() {
					public void run() {
						BluetoothGattService serv=mBluetoothGatt.getService(UUID.fromString("0000ffe0-0000-1000-8000-00805f9b34fb"));
						BluetoothGattCharacteristic config =serv.getCharacteristic(UUID.fromString("0000ffe1-0000-1000-8000-00805f9b34fb"));
						boolean ff=mBluetoothGatt.setCharacteristicNotification(config, true);
						Log.d("BTLE", "NOTIF:BOTONES:"+ff);
						BluetoothGattDescriptor configura = config.getDescriptor(CCC);
						byte[] configValue = ENABLE_NOTIFICATION_VALUE;
						boolean success = configura.setValue(configValue);
						Log.d("BTLE", "escrita notificacion BTN"+success);
						mBluetoothGatt.writeDescriptor(configura);
					}
				});
			}
		}
	}
	
	private void digestBotones(BluetoothGattCharacteristic characteristic) {
		final int valorBoton=characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8,0);
		runOnUiThread(new Runnable() {
		    @Override
		    public void run() {
		       switch (valorBoton) {
			case 1:
				lblBotones.setText("[  ] [X]");
				break;
			case 2:
				lblBotones.setText("[X] [  ]");
				break;
			case 3:
				lblBotones.setText("[X] [X]");
				break;
			default:
				lblBotones.setText("[  ] [  ]");
				break;
			}
		    	
		       }
		});
	}

}
