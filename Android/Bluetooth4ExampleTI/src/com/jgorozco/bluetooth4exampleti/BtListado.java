package com.jgorozco.bluetooth4exampleti;

import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothProfile;
import android.bluetooth.BluetoothAdapter.LeScanCallback;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;

@SuppressLint("NewApi")
public class BtListado extends Activity {
	List<String> devicesName;
	ArrayList<BluetoothDevice>devices;
	ListView listado;
	ArrayAdapter<String> adapterArray;
	BluetoothAdapter mBluetoothAdapter;
	
	
	
	@Override
	protected void onPause() {
	//	mBluetoothAdapter.disable();
		mBluetoothAdapter.stopLeScan(null);
		super.onPause();
	}
	@Override
	protected void onStart() {
	//	mBluetoothAdapter.enable();
		super.onStart();
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_bt_listado);
		devicesName=new ArrayList<String>();
		devices=new ArrayList<BluetoothDevice>();
		listado=(ListView) findViewById(R.id.listView1);
		adapterArray=new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,
				devicesName);
		 mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
		mBluetoothAdapter.enable();

        listado.setAdapter(adapterArray);
        listado.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				dispositivoSelecionado(arg2);
				
			}
		});
	}
	private BroadcastReceiver myReceiver = new BroadcastReceiver() {
	    public void onReceive(Context context, Intent intent) {
	    String action = intent.getAction();
	    if (BluetoothDevice.ACTION_FOUND.equals(action)) {
	        BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
	        Log.d("DEVICES", "FOUNDD::"+device.toString());
			 devicesName.add(device.getName());
			 devices.add(device);
	        updateAdapter();
	    }
	    if (BluetoothAdapter.ACTION_DISCOVERY_STARTED.equals(action)) {
	        Log.d("DEVICES", "Started discovery");
	    }
	    if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)) {
	        Log.d("DEVICES", "Finished discovery");
	    }
	}
	};

	public void clickBTNormal(View target){
		IntentFilter filterFound = new IntentFilter(BluetoothDevice.ACTION_FOUND);
		IntentFilter filterStart = new IntentFilter(BluetoothAdapter.ACTION_DISCOVERY_STARTED);
		IntentFilter filterStop = new IntentFilter(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
		registerReceiver(myReceiver, filterFound);
		registerReceiver(myReceiver, filterStart);
		registerReceiver(myReceiver, filterStop);
		mBluetoothAdapter.startDiscovery();
	}

	public void clickBT4Normal(View target){
		mBluetoothAdapter.startLeScan(new LeScanCallback() {
			@Override
			public void onLeScan(BluetoothDevice arg0, int arg1, byte[] arg2) {
				 Log.d("DEVICES", "FOUNDDD:::"+ arg0.getName());
				 devicesName.add(arg0.getName());
				 devices.add(arg0);
				 updateAdapter();
			}
		});
		
	}
	
	private void dispositivoSelecionado(int arg2) {
		BluetoothDevice dev=devices.get(arg2);
		Log.d("DEVICES","Seleccionado:"+String.valueOf(arg2)+"::"+dev.getName());
		dev.connectGatt(this, false, new BluetoothGattCallback() {
			
			@Override
			public void onReadRemoteRssi (BluetoothGatt gatt, int rssi, int status){
				List<BluetoothGattService> services = gatt.getServices();
				Log.d("DEVICES","SERVICIOS::"+services.size());
				for (BluetoothGattService serv :services){
					Log.d("DEVICES", "____________________________________");
					Log.d("DEVICES", "Servicio::"+serv.getUuid().toString());
					List<BluetoothGattCharacteristic> characts = serv.getCharacteristics();
					for (BluetoothGattCharacteristic chars : characts){
						Log.d("DEVICES", "  Char::"+chars.getUuid().toString());
					}
					Log.d("DEVICES", "____________________________________");
				}
				
			}
			
			@Override
			public void onConnectionStateChange (BluetoothGatt gatt, int status, int newState){
				Log.d("DEVICES","ESTADO BT::"+newState);
				if (newState==BluetoothProfile.STATE_CONNECTED){
				
				List<BluetoothGattService> services = gatt.getServices();
				Log.d("DEVICES","SERVICIOS::"+services.size());
				for (BluetoothGattService serv :services){
					Log.d("DEVICES", "____________________________________");
					Log.d("DEVICES", "Servicio::"+serv.getUuid().toString());
					List<BluetoothGattCharacteristic> characts = serv.getCharacteristics();
					for (BluetoothGattCharacteristic chars : characts){
						Log.d("DEVICES", "  Char::"+chars.getUuid().toString());
					}
					Log.d("DEVICES", "____________________________________");
				}
				}
				super.onConnectionStateChange(gatt, status,newState);
			}
			
			@Override
			public void onServicesDiscovered(BluetoothGatt gatt, int status) {
				List<BluetoothGattService> services = gatt.getServices();
				
				for (BluetoothGattService serv :services){
					Log.d("DEVICES", "____________________________________");
					Log.d("DEVICES", "Servicio::"+serv.getUuid().toString());
					List<BluetoothGattCharacteristic> characts = serv.getCharacteristics();
					for (BluetoothGattCharacteristic chars : characts){
						Log.d("DEVICES", "  Char::"+chars.getUuid().toString());
					}
					Log.d("DEVICES", "____________________________________");
				}
				
				super.onServicesDiscovered(gatt, status);
			}
			@Override
			public void onCharacteristicChanged (BluetoothGatt gatt, BluetoothGattCharacteristic characteristic){
				 Log.d("DEVICES", "onCharacteristicChanged discovery");
				
			}
			
		});
	}
	
	

	public void updateAdapter(){
		runOnUiThread(new Runnable() {
	        @Override
	        public void run() {
	        	adapterArray.notifyDataSetChanged();
	        }//public void run() {
		});
	}
	public void clickConectar(View target){}
}
