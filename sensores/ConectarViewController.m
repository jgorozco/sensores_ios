//
//  ConectarViewController.m
//  sensores
//
//  Created by jose garcia on 25/08/13.
//  Copyright (c) 2013 jose garcia. All rights reserved.
//

#import "ConectarViewController.h"
#import "BLEUtility.h"
#import "Sensors.h"
@interface ConectarViewController ()

@end

@implementation ConectarViewController
@synthesize periferico,imagenMostrada;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    imagenMostrada=NO;
    [self.photoView setHidden:YES];
    [self.nombreDispositivo setText:self.periferico.name];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)clickConectar:(id)sender {
 //  self.manager= [[CBCentralManager alloc]initWithDelegate:self queue:nil];
    self.manager.delegate=self;
    self.periferico.delegate=self;
    [self.manager connectPeripheral:self.periferico options:nil];
    
    gyro=[[sensorIMU3000 alloc] init];
    magSensor=[[sensorMAG3110 alloc] init];
    baroSensor=[[sensorC953A alloc]init];
}

- (IBAction)clickImageView:(id)sender {
    imagenMostrada=YES;
    if (self.switchFoto.isOn){
        [self.imageResize setImage:[UIImage imageNamed:@"logo_negro.png"]];
    }else{
        [self.imageResize setImage:[UIImage imageNamed:@"glass_team_pano.png"]];
    }
    [self.photoView setHidden:NO];

}

- (IBAction)clickClose:(id)sender {
    imagenMostrada=NO;
    [self.photoView setHidden:YES];
    self.imageResize.transform = CGAffineTransformIdentity;
}

-(void)girarImagen:(float)x and_y:(float)y and_z:(float)z{
    if (imagenMostrada){
        float degrees = 90.0*x;
        float zoom=(-z*0.5)+1;
        if(self.switchGiro.isOn){
            self.imageResize.transform = CGAffineTransformMakeRotation(degrees * M_PI/180);
        }else{
            self.imageResize.transform = CGAffineTransformMakeTranslation(degrees*4, 0);
        }
        self.imageResize.transform=CGAffineTransformScale(self.imageResize.transform, zoom, zoom);
    }
}

-(void)centralManagerDidUpdateState:(CBCentralManager *)central {
    if (central.state != CBCentralManagerStatePoweredOn) {
        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"BLE not supported !" message:[NSString stringWithFormat:@"CoreBluetooth return state: %d",central.state] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
    }
    else {
        [central scanForPeripheralsWithServices:nil options:nil];
    }
}



- (void)peripheral:(CBPeripheral *)peripheral didDiscoverCharacteristicsForService:(CBService *)service error:(NSError *)error{
    
    //NSLog(@"PERIFERICO %@ name[%@] is?%d  ERR%@",CFUUIDCreateString(nil, peripheral.UUID),peripheral.name,peripheral.,error);
    [BLEUtility configureService:service];
    peripheral.delegate=self;
}
/*
- (void)peripheral:(CBPeripheral *)peripheral didDiscoverIncludedServicesForService:(CBService *)service error:(NSError *)error{
    NSLog(@"didDiscoverIncludedServicesForService---->%@   ERR%@",CFUUIDCreateString(nil, peripheral.UUID),error.localizedDescription);

}
*/

-(void)peripheral:(CBPeripheral *)peripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^ {
        

    
    NSString *str=[BLEUtility hexRepresentationWithSpaces_AS:characteristic.value space:YES];
    NSString *charact=[BLEUtility CBUUIDToString:characteristic.UUID];
    NSMutableDictionary *d=[BLEUtility makeSensorTagConfiguration];
        NSString *uuidAcelService=[BLEUtility getKeyfor:@"Accelerometer service UUID" fromDic:d];//[d objectForKey:@"Accelerometer service UUID"];
    NSString *uuidTemperatureService=[BLEUtility getKeyfor:@"IR temperature service UUID" fromDic:d];//[d objectForKey:@"IR temperature service UUID"];
    NSString *uuidGiroscopioService=[BLEUtility getKeyfor:@"Gyroscope service UUID" fromDic:d];//[d objectForKey:@"Gyroscope service UUID"];
    NSString *uuidBarometroService=[BLEUtility getKeyfor:@"Barometer service UUID" fromDic:d];//[d objectForKey:@"Barometer service UUID"];
    NSString *uuidMagnetometorService=[BLEUtility getKeyfor:@"Magnetometer service UUID" fromDic:d];//[d objectForKey:@"Magnetometer service UUID"];
    NSString *uuidHumedadService=[BLEUtility getKeyfor:@"Humidity service UUID" fromDic:d];//[d objectForKey:@"Humidity service UUID"];
    if      ([[CBUUID UUIDWithString:uuidAcelService]isEqual:characteristic.service.UUID]){
        NSString *str=[NSString stringWithFormat:@"[% 0.1fG]X [% 0.1fG]Y [% 0.1fG]Z",[sensorKXTJ9 calcXValue:characteristic.value],
                       [sensorKXTJ9 calcYValue:characteristic.value],
                       [sensorKXTJ9 calcZValue:characteristic.value]];
        [self.lblAcelerometro setText:str];
        float x=[sensorKXTJ9 calcXValue:characteristic.value];
        float y=[sensorKXTJ9 calcYValue:characteristic.value];
        float z=[sensorKXTJ9 calcZValue:characteristic.value];
        [self girarImagen:x and_y:y and_z:z];

    }else if([[CBUUID UUIDWithString:uuidTemperatureService]isEqual:characteristic.service.UUID]){
        float tAmb = [sensorTMP006 calcTAmb:characteristic.value];
        float tObj = [sensorTMP006 calcTObj:characteristic.value];
        NSString *str=[NSString stringWithFormat:@"[%.1f°C]EXT [%.1f°C]INT",tAmb,tObj];
        [self.lblTemperatura setText:str];
    }else if([[CBUUID UUIDWithString:uuidGiroscopioService]isEqual:characteristic.service.UUID]){
   //     NSLog(@"GIROSCOPIO [%@]",str);

        NSString *str=[NSString stringWithFormat:@"[% 0.1f]X [% 0.1f]Y [% 0.1f]Z",[gyro calcXValue:characteristic.value],[gyro calcYValue:characteristic.value],[gyro calcZValue:characteristic.value]];
        [self.lblGiroscopio setText:str];

    }else if([[CBUUID UUIDWithString:uuidBarometroService]isEqual:characteristic.service.UUID]){
  //      NSLog(@"BAROMETRO [%@]",str);
        int pressure = [baroSensor calcPressure:characteristic.value];
        [self.lblBarometro setText:[NSString stringWithFormat:@"PRES:[%d]",pressure]];
    }else if([[CBUUID UUIDWithString:uuidMagnetometorService]isEqual:characteristic.service.UUID]){
 //       NSLog(@"MAGNETOMETRO [%@]",str);
        float x = [magSensor calcXValue:characteristic.value];
        float y = [magSensor calcYValue:characteristic.value];
        float z = [magSensor calcZValue:characteristic.value];
        NSString *str=[NSString stringWithFormat:@"[% 0.1f]X [% 0.1f]Y [% 0.1f]Z",x,y,z];
        [self.lblMagnetometro setText:str];
   //     [BLEUtility configureMagnetometro:service withConfig:d];
    }else if([[CBUUID UUIDWithString:uuidHumedadService]isEqual:characteristic.service.UUID]){
        NSLog(@"HUMEDAD [%@]",str);
         float rHVal = [sensorSHT21 calcPress:characteristic.value];
        [self.lblHumedad setText:[NSString stringWithFormat:@"%0.1f%%rH",rHVal]];
   //     [BLEUtility configureHumedad:service withConfig:d];
    }
    else{
        NSLog(@"----NO LO ES [%@] [%@] [%@]",[BLEUtility CBUUIDToString:[BLEUtility expandToTIUUID:characteristic.service.UUID]],charact,str);
        int valor=[str intValue];
        NSString *botonera=@"[  ] [  ]";
        switch (valor) {
            case 2:
                botonera=@"[X] [  ]";
                break;
            case 1:
                botonera=@"[  ] [X]";
                break;
            case 3:
                botonera=@"[X] [X]";
                break;
            default:
                botonera=@"[  ] [  ]";
                break;
        }
        [self.lblBotones setText:botonera];
   //     [self configureDefault:service];
    }
    }];
    
  //  NSLog(@":UPDATE VALUE:%@ :-> %@",charact,str);
}




-(void)centralManager:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)peripheral {
    peripheral.delegate = self;
    [peripheral discoverServices:nil];
}


-(void) peripheral:(CBPeripheral *)peripheral didDiscoverServices:(NSError *)error {
    NSLog(@"Services scanned ![%@] ERR:%@",peripheral.name,error);
    for (CBService *s in peripheral.services) [peripheral discoverCharacteristics:nil forService:s];
}

-(void) peripheral:(CBPeripheral *)peripheral didUpdateNotificationStateForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error {
    NSString *charact=CFBridgingRelease(CFUUIDCreateString(nil, (__bridge CFUUIDRef)(characteristic.UUID)));


    NSLog(@"UPDATE STATUS:%@ error = %@",charact,error.localizedDescription);
}

-(void) peripheral:(CBPeripheral *)peripheral didWriteValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error {
    NSLog(@"didWriteValueForCharacteristic %@ error = %@",characteristic,error);
}

@end
