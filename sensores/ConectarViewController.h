//
//  ConectarViewController.h
//  sensores
//
//  Created by jose garcia on 25/08/13.
//  Copyright (c) 2013 jose garcia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreBluetooth/CoreBluetooth.h>
#import "Sensors.h"
@interface ConectarViewController : UIViewController <CBCentralManagerDelegate,CBPeripheralDelegate>{
        sensorIMU3000 *gyro;
    sensorC953A *baroSensor;
    sensorMAG3110 *magSensor;
}


@property (nonatomic,retain)CBPeripheral *periferico;
@property (assign)BOOL imagenMostrada;
@property (strong,nonatomic) CBCentralManager *manager;
- (IBAction)clickConectar:(id)sender;
- (IBAction)clickImageView:(id)sender;
- (IBAction)clickClose:(id)sender;

@property (weak, nonatomic) IBOutlet UILabel *nombreDispositivo;

@property (weak, nonatomic) IBOutlet UILabel *lblAcelerometro;
@property (weak, nonatomic) IBOutlet UILabel *lblGiroscopio;
@property (weak, nonatomic) IBOutlet UILabel *lblTemperatura;
@property (weak, nonatomic) IBOutlet UILabel *lblBotones;
@property (weak, nonatomic) IBOutlet UILabel *lblMagnetometro;
@property (weak, nonatomic) IBOutlet UILabel *lblBarometro;
@property (weak, nonatomic) IBOutlet UILabel *lblHumedad;

@property (weak, nonatomic) IBOutlet UIView *photoView;
@property (weak, nonatomic) IBOutlet UIImageView *imageResize;
@property (weak, nonatomic) IBOutlet UISwitch *switchFoto;
@property (weak, nonatomic) IBOutlet UISwitch *switchGiro;

@end
